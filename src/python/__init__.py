"""
SPDX-License-Identifier: LGPL-3.0-or-later
Copyright (c) 2013-2022 XVM Team
"""



#
# Imports
#

# CPython
import compileall
import glob
import json
import importlib
import logging
import platform
import re
import sys

# BigWorld
import ResMgr
from helpers import VERSION_FILE_PATH

# XFW
import openwg_vfs as vfs
from .dag import DAG, DAGValidationError



#
# Exports
#

__all__ = [
    'XFWLOADER_PATH_TO_ROOT',
    'XFWLOADER_PACKAGES_REALFS',
    'XFWLOADER_PACKAGES_VFS',
    'XFWLOADER_TEMPDIR',
    'WOT_RESMODS_DIR',
    'WOT_VERSION_FULL',
    'WOT_VERSION_SHORT',

    'get_client_realm'

    'get_mod_directory_name',
    'get_mod_directory_path',
    'get_mod_user_data'
    'get_mod_module',
    'get_mod_ids',

    'is_mod_exists',
    'is_mod_in_realfs',
    'is_mod_loaded'
]



#
# Globals
#

XFWLOADER_PATH_TO_ROOT    = None
XFWLOADER_PACKAGES_REALFS = None
XFWLOADER_PACKAGES_VFS    = None
XFWLOADER_TEMPDIR         = None

WOT_RESMODS_DIR = None
WOT_VERSION_FULL = None
WOT_VERSION_SHORT = None


def __globals_init():
    global XFWLOADER_PATH_TO_ROOT
    XFWLOADER_PATH_TO_ROOT = ''

    global XFWLOADER_PACKAGES_REALFS
    XFWLOADER_PACKAGES_REALFS = XFWLOADER_PATH_TO_ROOT + u'res_mods/mods/xfw_packages'

    global XFWLOADER_PACKAGES_VFS
    XFWLOADER_PACKAGES_VFS = u'mods/xfw_packages'

    global XFWLOADER_TEMPDIR
    XFWLOADER_TEMPDIR = XFWLOADER_PATH_TO_ROOT + u'mods/temp'

    # "../res_mods/0.9.20.1/""
    global WOT_RESMODS_DIR
    WOT_RESMODS_DIR = XFWLOADER_PATH_TO_ROOT + [path.asString for path in ResMgr.openSection('../paths.xml')['Paths'].values() if './res_mods/' in path.asString][0].lstrip('./')

    # ver =
    #   * 'v.0.8.7'
    #   * 'v.0.8.7 #512'
    #   * 'v.0.8.7 Common Test #499'
    #   * 'Supertest v.ST 0.9.15.1 #366'
    #   * 'Supertest v.1.5.1.0 #546'
    ver = ResMgr.openSection(VERSION_FILE_PATH).readString('version')
    if 'Supertest v.ST' in ver:
        tokens = ver.split(" ", 2)
        ver = tokens[2] + ' ' + tokens[1]
    elif 'Supertest v.' in ver:
        tokens = ver.split(' ', 1)
        ver = '%s %s' % (tokens[1][2:tokens[1].index('#') - 1], tokens[0])
    elif '#' in ver:
        ver = ver[2:ver.index('#') - 1]
    else:
        ver = ver[2:]

    short_ver = ver if not ' ' in ver else ver[:ver.index(' ')]  # X.Y.Z or X.Y.Z.a

    global WOT_VERSION_FULL
    WOT_VERSION_FULL = ver

    global WOT_VERSION_SHORT
    WOT_VERSION_SHORT = short_ver


#
# Helpers
#

def __get_architecture_name():
    arch = platform.architecture()[0]
    if arch == "32bit":
        return "x86_32"
    if arch == "64bit":
       return "x86_64"
    return arch


def __get_keys_by_mask(dict, mask):
    keys_found = list()

    prefix = mask.split('*', 1)[0]
    for key in dict:
        if key.startswith(prefix):
            keys_found.append(key)

    return keys_found


def __compare_versions(version1, version2):
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$','', v.split(" ", 1)[0]).split(".")]
    return cmp(normalize(version1), normalize(version2))



#
# DAG
#

def __dag_add_edge(dag, u, v):
    try:
        dag.add_node_if_not_exists(u)
        dag.add_node_if_not_exists(v)
        dag.add_edge(u, v)
    except DAGValidationError:
        return False

    return True


def __dag_build(mods, mods_features):
    dag = DAG()

    for mod_id, mod_config in mods.iteritems():
        dependency_added = False

        #dependencies
        if 'dependencies' in mod_config:
            for dependency in mod_config['dependencies']:
                result = __dag_add_edge(dag, dependency, mod_id)
                if result:
                    dependency_added = True

        #features
        if 'features' in mod_config:
            for feature in mod_config['features']:
                if feature in mods_features:
                    result = __dag_add_edge(dag, mods_features[feature], mod_id)
                    if result:
                        dependency_added = True

        if not dependency_added:
            __dag_add_edge(dag, 'root', mod_id)

        #optional dependencies
        if 'dependencies_optional' in mod_config:
            for dependency in mod_config['dependencies_optional']:
                if '*' in dependency:
                    for dependency in __get_keys_by_mask(mods, dependency):
                        __dag_add_edge(dag, dependency, mod_id)
                else:
                    __dag_add_edge(dag, dependency, mod_id)

    return dag



#
# Mods
#

__mods_inited   = False
__mods_info     = dict()
__mods_failed   = list()
__mods_loaded   = list()


def __mods_get_realfs():
    """
    fills mods list with modifications in realfs
    path to search: [WoT]/res_mods/mods/xfw_packages/*/xfw_package.json
    """
    logger = logging.getLogger('XFW/Loader')
    m_configs = [i.replace("\\", "/").replace("//", "/") for i in glob.iglob(XFWLOADER_PACKAGES_REALFS + '/*/xfw_package.json')]
    for m_config in m_configs:
        m_dir = m_config[0:m_config.rfind("/")] # module directory

        try:
            with open(m_config) as m_config_f:
                data = json.load(m_config_f)

                if data['id'] in __mods_info.keys():
                    logger.warning("mods_get_realfs: mod '%s' was already found" % data['id'])
                    logger.warning("                 current location  : %s" % m_dir)
                    logger.warning("                 imported location : %s" % __mods_info[data['id']]['dir_path'])
                else:
                    __mods_info[data['id']] = data
                    __mods_info[data['id']]['fs'] = 'realfs'
                    __mods_info[data['id']]['dir_path'] = m_dir
                    __mods_info[data['id']]['dir_name'] = m_dir[m_dir.rfind("/")+1:]

        except Exception:
            logger.exception("mods_get_realfs: Could not parse config for directory '%s'" % m_dir)


def __mods_get_vfs():
    """
    fills mods list with  modifications in vfs
    path to search: [VFS_root]/mods/xfw_packages/*/xfw_package.json
    """

    logger = logging.getLogger('XFW/Loader')
    for m_dir_name in vfs.directory_list_subdirs(XFWLOADER_PACKAGES_VFS):
        try:
            m_dir = XFWLOADER_PACKAGES_VFS + '/' + m_dir_name

            mod_config = vfs.file_read(m_dir + '/xfw_package.json', True)
            if mod_config is not None:
                data = json.loads(mod_config)

                if data['id'] in __mods_info.keys():
                    logger.warning("mods_get_vfs: Error: mod '%s' was already found" % data['id'])
                    logger.warning("              current location  : %s" % m_dir)
                    logger.warning("              imported location : %s" % __mods_info[data['id']]['dir_path'])
                else:
                    __mods_info[data['id']] = data
                    __mods_info[data['id']]['fs'] = 'vfs'
                    __mods_info[data['id']]['dir_path'] = m_dir
                    __mods_info[data['id']]['dir_name'] = m_dir_name

        except Exception:
            logger.exception("mods_get_vfs: Could not parse config for directory '%s'" % m_dir)


def __mods_init():
    """
    Loads XFW-powered mods from work_folder
    """

    logger = logging.getLogger('XFW/Loader')

    #set globals
    global __mods_info
    global __mods_failed
    global __mods_loaded
    global __mods_inited

    # check that we are not loading mods second time
    if __mods_inited:
        logger.error("mods_init: Mods were already loaded")
        return

    # read FS
    __mods_get_realfs()
    __mods_get_vfs()

    # update path
    if XFWLOADER_PACKAGES_VFS not in sys.path:
        sys.path.insert(0, XFWLOADER_PACKAGES_VFS)
    if XFWLOADER_PACKAGES_REALFS not in sys.path:
        sys.path.insert(0, XFWLOADER_PACKAGES_REALFS)

    if not any(__mods_info):
        logger.warning("mods_init: No mods were found")
        return

    #parse features
    mods_features = dict()
    for mod_id, mod_config in __mods_info.iteritems():
        if 'features_provide' in mod_config:
            for provided_feature in mod_config['features_provide']:
                mods_features[provided_feature] = mod_id


    #build DAG
    mods_dag = __dag_build(__mods_info, mods_features)

    # load modifications in topological order
    for mod_name in mods_dag.topological_sort():
        if mod_name == "root":
            continue

        #validate:
        if mod_name not in __mods_info:
            logger.warning("mods_init: Error with mod: '%s'. Mod not found" % mod_name)
            __mods_failed.append(mod_name)
            continue

        if mod_name in __mods_failed:
            logger.warning("mods_init: Error with mod: '%s'. Mod was marked as failed" % mod_name)
            continue

        mod = __mods_info[mod_name]
        if mod is None:
            logger.warning("mods_init: Error with mod: '%s'. Mod info object is None" % mod_name)
            __mods_failed.append(mod_name)
            continue

        # check version
        if 'wot_version_min' in mod and len(mod['wot_version_min']) > 0:
            compare_result = __compare_versions(WOT_VERSION_SHORT, mod['wot_version_min'])
            if compare_result < 0:
                logger.warning("mods_init: Error with mod: '%s'. Client version is lower than required: current: '%s', required: '%s'" % (mod_name, WOT_VERSION_SHORT, mod['wot_version_min']))
                __mods_failed.append(mod_name)
                continue

            if compare_result > 0 and 'wot_version_exactmatch' in mod and mod['wot_version_exactmatch'] == True:
                logger.warning("mods_init: Error with mod: '%s'. Client version is higher than required: current: '%s', required: '%s'" % (mod_name, WOT_VERSION_SHORT, mod['wot_version_min']))
                __mods_failed.append(mod_name)
                continue

        # check architecture
        failed = False
        if 'architecture' in mod:
            failed = True
            for arch in mod['architecture']:
                if arch == __get_architecture_name():
                    failed = False

        if failed == True:
            logger.warning("mods_init: Error with mod: '%s'. Current architecture is not supported: '%s'" % (mod_name, platform.architecture()[0]))
            __mods_failed.append(mod_name)
            continue

        # check features
        failed = False
        if 'features' in mod:
            for feature in mod['features']:
                if feature not in mods_features:
                    logger.warning("mods_init: Error with mod: '%s'. Feature not found: '%s'" % (mod_name, feature))
                    failed = True
                    continue

                if mods_features[feature] == mod_name:
                    continue

                if mods_features[feature] in __mods_failed:
                    logger.warning("mods_init: Error with mod: '%s'. Feature failed: '%s' in package '%s'" % (mod_name, feature, mods_features[feature]))
                    failed = True

        if failed == True:
            __mods_failed.append(mod_name)
            continue

        #check dependencies
        if 'dependencies' in mod:
            for dependency in mod['dependencies']:
                if dependency not in __mods_info:
                    logger.warning("mods_init: Error with mod: '%s'. Dependency not found: '%s'" % (mod_name, dependency))
                    failed = True
                    continue

                if dependency in __mods_failed:
                    logger.warning("mods_init: Error with mod: '%s'. Dependency failed: '%s'" % (mod_name, dependency))
                    failed = True

        if failed == True:
            __mods_failed.append(mod_name)
            continue

        #load
        logger.info("mods_init: Loading mod: %s, v. %s" % (mod_name, mod['version']))

        #check for features
        if 'features' in mod:

            #load `python` feature
            if 'python' in mod['features']:
                try:
                    if mod['fs'] == 'realfs':
                        open(mod['dir_path'] + '/__init__.py', 'a').close()
                        compileall.compile_dir(mod['dir_path'], quiet = 1)

                    #try to load module
                    module = importlib.import_module('%s.python' % mod['dir_name'])

                    #call `xfw_module_init()`
                    if hasattr(module, 'xfw_module_init'):
                        module.xfw_module_init()

                    #check xfw_is_module_loaded() function if module provides it
                    if hasattr(module, 'xfw_is_module_loaded'):
                        if not module.xfw_is_module_loaded():
                            logger.error("mods_init: Loading mod: '%s' FAILED (flag)" % mod_name)
                            failed = True

                except Exception:
                    logger.exception("mods_init: Loading mod: '%s' FAILED (exception)" % mod_name)
                    failed = True

            #load `python_libraries` feature
            if not failed:
                if 'python_libraries' in mod['features']:
                    sys.path.insert(0, '%s/python_libraries' % mod['dir_path'])

        #add mod to loaded list
        if not failed:
            __mods_loaded.append(mod_name)
        else:
            __mods_failed.append(mod_name)

    __mods_inited = True


def __mods_fini():
    for mod_name in reversed(__mods_loaded):
        try:
            mod_info = __mods_info[mod_name]
            if 'python' in mod_info['features']:
                mod_module = importlib.import_module('%s.python' % mod_info['dir_name'])
                if hasattr(mod_module, 'xfw_module_fini'):
                    mod_module.xfw_module_fini()  
        except:
            logging.getLogger(mod_name).exception('mods_fini: mod = %s' % mod_name, exc_info=True)


def __mods_event(eventName, *args, **kwargs):
    for mod_name in __mods_loaded:
        try:
            mod_info = __mods_info[mod_name]
            if 'python' in mod_info['features']:
                mod_module = importlib.import_module('%s.python' % mod_info['dir_name'])
                if hasattr(mod_module, 'xfw_module_event'):
                    mod_module.xfw_module_event(eventName, *args, **kwargs)  
        except:
            logging.getLogger(mod_name).exception('mods_event: mod = %s, event = %s' % (mod_name, eventName), exc_info=True)



#
# Public
#

def get_client_realm():
    return importlib.import_module('constants').CURRENT_REALM


def get_mod_directory_name(mod_name):
    if mod_name not in __mods_info:
        return None
    return __mods_info[mod_name]['dir_name']


def get_mod_directory_path(mod_name):
    if mod_name not in __mods_info:
        return None
    return __mods_info[mod_name]['dir_path']


def get_mod_user_data(package_id, var_id):
    if package_id not in __mods_info:
        return None

    if 'user_data' not in __mods_info[package_id]:
        return None

    if var_id not in __mods_info[package_id]['user_data']:
        return None

    return __mods_info[package_id]['user_data'][var_id]


def get_mod_module(mod_name):
    if mod_name not in __mods_loaded:
        return None
    return importlib.import_module('%s.python' % __mods_info[mod_name]['dir_name'])


def get_mod_ids():
    result = dict()
    for mod_id, mod_config in __mods_info.iteritems():
        result[mod_id] = mod_config['version']
    return result


def is_mod_exists(mod_name):
    return mod_name in __mods_info


def is_mod_loaded(mod_name):
    return mod_name in __mods_loaded


def is_mod_in_realfs(mod_name):
    if mod_name not in __mods_info:
        return False

    return __mods_info[mod_name]['fs'] == 'realfs'



#
# WG Mod API
#

def init():
    __globals_init()
    __mods_init()


def fini():
    __mods_fini()


def event(eventName, *args, **kwargs):
    __mods_event(eventName, *args, **kwargs)
