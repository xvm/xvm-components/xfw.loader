"""
SPDX-License-Identifier: LGPL-3.0-or-later
Copyright (c) 2013-2022 XVM Team
"""



#
# Imports
#

# cpython
import logging
import sys



#
# Wargaming API
#

def init():
    try:
        # update sys.path, realfs has priority over VFS
        sys.path.insert(0, 'mods/xfw_packages')
        sys.path.insert(0, 'res_mods/mods/xfw_packages')
        
        import xfw_loader.python as loader
        loader.init()
    except Exception:
        logging.getLogger('XFW/Entrypoint').exception('init:', exc_info=True)


def fini():
    try:
        import xfw_loader.python as loader
        loader.fini()
    except:
        logging.getLogger('XFW/Entrypoint').exception('fini', exc_info=True)


def onAccountBecomePlayer(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onAccountBecomePlayer', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onAccountBecomePlayer', exc_info=True)


def onAccountBecomeNonPlayer(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onAccountBecomeNonPlayer', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onAccountBecomePlayer', exc_info=True)


def onAvatarBecomePlayer(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onAvatarBecomePlayer', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onAvatarBecomePlayer', exc_info=True)


def onConnected(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onConnected', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onConnected', exc_info=True)


def onDisconnected(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onDisconnected', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onDisconnected', exc_info=True)


def onAccountShowGUI(*args, **kwargs):
    try:
        import xfw_loader.python as loader
        loader.event('onAccountShowGUI', *args, **kwargs)
    except:
        logging.getLogger('XFW/Entrypoint').exception('onAccountShowGUI', exc_info=True)
