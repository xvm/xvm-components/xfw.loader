# Changelog

## v10.2.3 (2024.10.15)

* fix realm detection

## v10.2.2 (2024.04.15)

* removed debug output

## v10.2.1 (2024.04.14)

* `vfs` submodule was moved to OpenWG project

## v10.2.0 (2022.08.18)

* fix game events processing for modules without `python` features
* introduce `python_libraries` feature

## v10.1.0 (2022.08.15)

* introduce `xfw_module_fini()`
* introduce `xfw_module_event()`


## v10.0.0 (2022.08.12)

* first release as a separate project